### D'Accord project 

Pour installer l’application sur un serveur, se connecter au serveur et effectuer les commandes suivantes dans une console :

docker-compose up -d (installation de l’environnement et de l’application)

cp .env.exemple .env (renomme le fichier de configuration)

docker run --rm --tty --volume $PWD:/var/www/html composer install (installation des dépendances

docker-compose exec php vendor/bin/doctrine orm:schema-tool:update -f (mise à jour du schéma de la base de donnée)