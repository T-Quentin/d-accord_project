<?php
namespace App\Controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class HomeController extends Controller
{
    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     */
    public function getHome(RequestInterface $request, ResponseInterface $response)
    {
        $this->logger->addInfo('Nouvelle connexion');

        $nb = $this->em->getRepository('App\Entity\Counter')->queryNumber();
        $moyenne = $this->em->getRepository('App\Entity\Counter')->getMoyenne();
        $params = [
            'nb' => $nb,
            'moyenne' => $moyenne
        ];
        $this->render($response, 'pages/home.twig', $params);
    }

    public function postHome(RequestInterface $request, ResponseInterface $response)
    {
        return $this->redirect($response, 'home');
    }
}
