<?php
namespace App\Controllers;

use App\Entity\Counter;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;

class CounterController extends Controller
{

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return string
     */
    public function inc(RequestInterface $request, ResponseInterface $response)
    {

        $datetime =  new \DateTime();

        $checkPoint = new Counter('daccord', $datetime);

        $this->container->get('em')->persist($checkPoint);
        $this->container->get('em')->flush();

        $nb = $this->container->get('em')->getRepository('App\Entity\Counter')->queryNumber();

        $moyenne = $this->container->get('em')->getRepository('App\Entity\Counter')->getMoyenne();

        $newResponse = $response->withJson(['nb' => $nb, 'moyenne' => $moyenne]);

        return $newResponse;
    }

    public function raz(RequestInterface $request, ResponseInterface $response)
    {
        $logs = $this->container->get('em')->getRepository('App\Entity\Counter')->findAll();

        $em = $this->container->get("em");
        foreach ($logs as $log) {
            $em->remove($log);
        }
        $em->flush();

        $nb = $this->container->get('em')->getRepository('App\Entity\Counter')->queryNumber();

        $newResponse = $response->withJson($nb);

        return $newResponse;
    }
}
