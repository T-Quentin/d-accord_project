<?php

namespace App\Entity;

use Doctrine\ORM\EntityRepository;

class CounterRepository extends EntityRepository
{
    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function queryNumber()
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * return void
     */
    public function queryRAZ()
    {
        $this->createQueryBuilder('p')
            ->delete()
            ->getQuery()
            ->execute();

        return;
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMoyenne()
    {
        $nb = $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $min = $this->createQueryBuilder('p')
            ->select('MIN(p.datetime)')
            ->getQuery()
            ->getSingleScalarResult();

        $max = $this->createQueryBuilder('p')
            ->select('MAX(p.datetime)')
            ->getQuery()
            ->getSingleScalarResult();

        if ($nb == 0 || $min == 0 || $max == 0 || ($min == $max)) {
            return '∞';
        }

        return $result = round((date_timestamp_get(new \Datetime($max)) - date_timestamp_get(new \Datetime($min))) / $nb, 2);
    }
}