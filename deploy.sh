 #!/bin/bash

git pull
docker-compose up -d
docker run --rm --tty --volume $PWD:/app composer install
docker-compose exec -T php vendor/bin/doctrine orm:schema-tool:update -f