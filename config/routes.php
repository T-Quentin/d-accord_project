<?php

use App\Controllers\HomeController;
use App\Controllers\CounterController;

$app->get('/', HomeController::class. ':getHome')->setName('home');

$app->post('/inc', CounterController::class. ':inc')->setName('inc');

$app->post('/raz', CounterController::class. ':raz')->setName('raz');